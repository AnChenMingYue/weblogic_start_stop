#!/bin/bash
cruent_path=`pwd`
validate() {
    if [ ! -f "script_weblogic_ENV.sh" ] ; then
        echo  "There is no script_weblogic_ENV ,exit!"
        exit 1
    fi
}
source ${cruent_path}/script_weblogic_ENV.sh
source $scriptHOME/PARAMETER_SH/domains_$app_group.sh

. $scriptHOME/start_NodeManager_weblogic.sh

cd ${cruent_path}

. $scriptHOME/start_$app_group.sh
