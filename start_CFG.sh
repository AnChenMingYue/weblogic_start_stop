#!/bin/bash
validate() {
    if [ ! -f "script_weblogic_ENV.sh" ] ; then
        echo  "There is no script_weblogic_ENV ,exit!"
        exit 1
    fi
}
source script_weblogic_ENV.sh

. $scriptHOME/weblogicServerInstance/startServerInstance.sh $scriptHOME/PARAMETER_SH/PARAMETER_Server-8001.sh
. $scriptHOME/weblogicServerInstance/startServerInstance.sh $scriptHOME/PARAMETER_SH/PARAMETER_Server-EJB.sh
. $scriptHOME/weblogicServerInstance/startServerInstance.sh $scriptHOME/PARAMETER_SH/PARAMETER_Server-WebService.sh