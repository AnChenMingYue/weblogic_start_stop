#!/bin/bash
validate() {
    if [ ! -f "script_weblogic_ENV.sh" ] ; then
        echo  "There is no script_weblogic_ENV ,exit!"
        exit 1
    fi
}
source script_weblogic_ENV.sh
source $scriptHOME/PARAMETER_SH/domains_$app_group.sh

. $scriptHOME/weblogicAdminServer/stopAdminServer.sh