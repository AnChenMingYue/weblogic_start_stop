#!/bin/bash
source $scriptHOME/ocf-returncodes
source $scriptHOME/weblogicNodeManager/fuctionNodeManager.sh

validate
if [ $? -eq 1 ] ;
    then
        return $OCF_NOT_RUNNING
fi

status_NodeManager
if [ $? -eq $OCF_SUCCESS ]; then
		echo -e 'NodeManager ' 'aleady         ' $RUNNING
		return $OCF_SUCCESS
else
    
    start_NodeManager
	
    wait_start
fi