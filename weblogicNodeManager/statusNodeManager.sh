#!/bin/bash
source $scriptHOME/ocf-returncodes
source $scriptHOME/weblogicNodeManager/fuctionNodeManager.sh

validate
if [ $? -eq 1 ] ;
    then
        return $OCF_NOT_RUNNING
fi

status_NodeManager

if [ $? -eq $OCF_SUCCESS ] ; 
then
	echo -e 'Node Manager' 'is                  ' $RUNNING
    return $OCF_SUCCESS
else
	echo -e 'Node Manager' 'is                  ' $STOP 
    return $OCF_NOT_RUNNING
fi