#!/bin/bash
source $scriptHOME/ocf-returncodes
NodeManagerHOME=$weblogicHOME/wlserver_10.3/server/bin

start_NodeManager()
{
    cd ${NodeManagerHOME}
    nohup ./startNodeManager.sh&  > /dev/null
    start_code=$?
    return $start_code
}

stop_NodeManager()
{
    ps -ef  |grep 'weblogic.NodeManager' | grep -v 'script/weblogicNodeManager'  |grep -v 'grep' | awk '{print $2}' | while read pid   
    do
            echo "kill "$pid
            kill -9 $pid
    done
    stop_code=$?
    return $stop_code
}

status_NodeManager()
{
    ps -ef  |grep 'weblogic.NodeManager' | grep -v 'script/weblogicNodeManager' |grep -v 'grep' >/dev/null
    if [ $? -eq 1 ] ; 
    then
        return $OCF_NOT_RUNNING
    else
        return $OCF_SUCCESS
    fi
}

validate() {
    if [ ! -d "${NodeManagerHOME}" ] ; then
        echo  "The weblogic floder ${NodeManagerHOME} is not in this server!"
        exit 1
    fi
    return 0
}

wait_start()
{
	t=1
	while [ ${t} -le 10 ] 
	do
		
		status_NodeManager
		if [ $? -eq $OCF_SUCCESS ]; then
				echo -e 'NodeManager ' 'start         ' $SUCCESS_STRING
				return $OCF_SUCCESS
				
			else
				echo -e 'NodeManager ' 'is starting, wait.....' 
		fi
		sleep 2
		(( t++ ))
	done
	echo 'start timeout in ' 200 's'
}

wait_stop()
{
	t=1
	while [ ${t} -le 10 ] 
	do
 
		status_NodeManager
		if [ $? -eq $OCF_NOT_RUNNING  ];
			then
				echo -e 'NodeManager ' 'stop         ' $SUCCESS_STRING
				return $OCF_SUCCESS
			else
				echo -e 'NodeManager ' 'is stopping, wait.....' 
		fi
		sleep 2
		(( t++ ))
	done
	echo 'stop timeout in ' 200 's'
}