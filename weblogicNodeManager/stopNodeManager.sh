#!/bin/bash
source $scriptHOME/ocf-returncodes
source $scriptHOME/weblogicNodeManager/fuctionNodeManager.sh

validate
if [ $? -eq 1 ] ;
    then
        return $OCF_NOT_RUNNING
fi

status_NodeManager
if [ $? -eq $OCF_NOT_RUNNING ] ; 
then
	echo -e 'NodeManager ' 'stop         ' $SUCCESS_STRING
    return $OCF_SUCCESS
else
    stop_NodeManager
	wait_stop
fi
