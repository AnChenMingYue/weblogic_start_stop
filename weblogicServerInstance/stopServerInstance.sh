#!/bin/bash
source $1
#echo 'server PARAMETER:' $1
source $scriptHOME/ocf-returncodes
source $scriptHOME/weblogicServerInstance/fuctionServerInstanceTool.sh

 
# server_name=$1
# WLS_USER=$2
# WLS_PW=$3
# server_args="${server_name} ${WLS_USER} ${WLS_PW}"
server_args=$server_args_instance

validate
if [ $? -eq 1 ] ;
    then
        return $OCF_NOT_RUNNING
fi

status ${server_args}
check_status=$?
# echo 'stop $server_name ,check_status ' $check_status
if [ $check_status -eq $OCF_NOT_RUNNING ] ; 
then
	echo -e ${SERVER_NAME} 'stop         ' $SUCCESS_STRING
    return $OCF_SUCCESS
else
    stop_ServerInstance ${server_args}
    stop_ServerInstance_by_process ${server_args}  # 杀进程，保证关闭
    status ${server_args}
    check_status=$?
	if [ $check_status -eq $OCF_NOT_RUNNING  ];
		then
			echo -e ${SERVER_NAME} 'stop         ' $SUCCESS_STRING
			return $OCF_SUCCESS
		else
			echo -e ${SERVER_NAME} 'stop         ' $FAIL_STRING
			# return $OCF_ERR_GENERIC
		fi
fi
