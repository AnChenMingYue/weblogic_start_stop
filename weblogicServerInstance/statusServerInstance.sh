#!/bin/bash
source $1
# echo 'server PARAMETER:' $1
source $scriptHOME/ocf-returncodes
source $scriptHOME/weblogicServerInstance/fuctionServerInstanceTool.sh

# server_name=$1
# WLS_USER=$2
# WLS_PW=$3
# server_args="${server_name} ${WLS_USER} ${WLS_PW}"
server_args=$server_args_instance


validate
if [ $? -eq 1 ] ;
    then
        return $OCF_NOT_RUNNING
fi



status ${server_args}
if [ $? -eq $OCF_SUCCESS ] ; 
    then
		echo -e ${SERVER_NAME} 'is                  '  $RUNNING
        return $OCF_SUCCESS
    else
		echo -e ${SERVER_NAME} 'is                  ' $STOP 
        return $OCF_NOT_RUNNING
fi

