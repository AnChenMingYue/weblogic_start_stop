#!/bin/bash
source $scriptHOME/ocf-returncodes
WL_HOME=$weblogicHOME/wlserver_10.3
setWLSEnv_file=${WL_HOME}/server/bin/setWLSEnv.sh
source ${setWLSEnv_file} >/dev/null

DOMAIN_HOME="$weblogicHOME/user_projects/domains/$domains"
source ${DOMAIN_HOME}/bin/setDomainEnv.sh >/dev/null

start_ServerInstance()
{
    AdminHost=`cat /etc/hosts|grep $HOSTNAME | grep -v 127.0.0.1| awk '{print $1}'`
    SERVER_NAME=$1
    WLS_USER=$2
    WLS_PW=$3
    # echo 'start_ServerInstance args: ' $1 $2 $3 $4
    ${JAVA_HOME}/bin/java weblogic.Admin  \
    -adminurl localhost:7001 \
    -username ${WLS_USER} \
    -password ${WLS_PW} START ${SERVER_NAME} 
    return $?
}

stop_ServerInstance()
{
    AdminHost=`cat /etc/hosts|grep $HOSTNAME | grep -v 127.0.0.1| awk '{print $1}'`
    SERVER_NAME=$1
    WLS_USER=$2
    WLS_PW=$3
    # echo 'stop_ServerInstance args:' $1 $2 $3 $4
    ${JAVA_HOME}/bin/java weblogic.Admin  \
    -adminurl localhost:7001 \
    -username ${WLS_USER} \
    -password ${WLS_PW} FORCESHUTDOWN ${SERVER_NAME} 
    return $?
}

stop_ServerInstance_by_process()
{
    server_name=$1
    ps -ef |grep -v 'grep' |grep -v '/bin/bash' | grep 'weblogic.Server' | grep ${server_name} | awk '{print $2}' | while read pid    >/dev/null
        do
                echo "kill "$pid
                kill -9 $pid
        done

}

validate() {
    if [ ! -d "${WL_HOME}" ]  ; then
        echo  "The weblogic floder ${WL_HOME} is not in this server!"
        exit 1
    fi
    return 0
}


 status_by_weblogic_Admin()
{
    args=$@
    SERVER_NAME=$1
    WLS_USER=$2
    WLS_PW=$3
    AdminHost=`cat /etc/hosts|grep $HOSTNAME | grep -v 127.0.0.1| awk '{print $1}'`
    # echo 'status_by_weblogic_Admin args:' $1 $2 $3 $4
    ${JAVA_HOME}/bin/java weblogic.Admin  \
    -adminurl localhost:7001 \
    -username ${WLS_USER} \
    -password ${WLS_PW} GETSTATE ${SERVER_NAME} | awk '{print $6}' |while read status   
    do
        case $status in
            'RUNNING')
                # echo  ${SERVER_NAME} 'status: RUNNING' 
                return $OCF_SUCCESS
                ;;
            'SHUTDOWN')
                # echo  ${SERVER_NAME} 'status: SHUTDOWN'
                return $OCF_NOT_RUNNING
                ;; 
            'Destination')
                # echo 'AdminServer not running'
               return $OCF_ERR_UNIMPLEMENTED
                ;; 
            'STARTING')
                # echo  ${SERVER_NAME} 'status: STARTING'
                sleep 10s
                status_by_weblogic_Admin  ${args}
                ;; 
            *)     
                # echo ${SERVER_NAME} 'status: ' $status
               return $OCF_ERR_GENERIC
               ;; 
        esac
    done 

}


status_port()
{
    server_name=$1
    count_pid=`ps -ef |grep -v 'grep' |grep -v '/bin/bash' | grep 'weblogic.Server' | grep ${server_name} |wc -l `
    if [ ${count_pid}  -ge 1 ] 
        then
            pid=`ps -ef |grep -v 'grep' |grep -v '/bin/bash' | grep 'weblogic.Server' | grep ${server_name} |awk '{print $2}' `
            count_port=`netstat -nalp |grep -v 'grep' |grep -v '/bin/bash'  |grep 'LISTEN' |grep ${pid}  |wc -l`
            if [ ${count_port}  -ge 1 ] 
                then
                    return 0
                else
                    return 1
            fi 
    else
            return 1
    fi 
   
    
}

status_process()
{
    count_process=`ps -ef |grep -v 'grep' |grep -v '/bin/bash' | grep 'weblogic.Server' | grep $1 |wc -l `
    if [ ${count_process}  -eq 1 ] 
        then
            return 0
        else
            return 1
    fi 
}

status_by_process_port()
{
    SERVER_NAME=$1
    WLS_USER=$2
    WLS_PW=$3
    # echo 'status_by_process_port args:' $1 $2 $3 $4
    status_port ${SERVER_NAME}
    port_status_code=$?
    status_process ${SERVER_NAME}
    process_status_code=$?
    # echo 'status' ${SERVER_NAME} ': port_status_code' $port_status_code ': process_status_code'  $process_status_code
    if [ $port_status_code -eq 0 ] && [ $process_status_code -eq 0 ] ; then
		# echo ${SERVER_NAME} 'check by process port is \033[32m RUNNING \033[0m' 
        return $OCF_SUCCESS
    else
		# echo ${SERVER_NAME} 'check by process port is \033[32m STOP \033[0m ' 
        return $OCF_NOT_RUNNING
     fi
 }
 
 status(){
    args=$@
	
    #echo '----' $args
    
    status_by_weblogic_Admin $args
    check_by_weblogic_Admin=$?
    case ${check_by_weblogic_Admin} in
            $OCF_SUCCESS)
                return $OCF_SUCCESS
                ;;
            $OCF_NOT_RUNNING)
                return $OCF_NOT_RUNNING
                ;; 
            $OCF_ERR_UNIMPLEMENTED)
                status_by_process_port  $args
                check_by_process_port=$?
                if [ ${check_by_process_port} -eq $OCF_SUCCESS ] ; then
                         return $OCF_SUCCESS
                else
                    return $OCF_NOT_RUNNING
                 fi
                ;; 
            *)     
                status_by_process_port  $args
                check_by_process_port=$?
                if [ ${check_by_process_port} -eq $OCF_SUCCESS ] ; then
                         return $OCF_SUCCESS
                else
                    return $OCF_NOT_RUNNING
                 fi
                ;; 
        esac
 }
 wait_start()
{
	t=1
	while [ ${t} -le 10 ] 
	do
		
		status ${server_args}
		st=$?
		if [ $st -eq $OCF_SUCCESS ] ; 
			then
				echo -e ${SERVER_NAME} 'start         ' $SUCCESS_STRING
				return $OCF_SUCCESS
			else
				echo -e ${SERVER_NAME} 'starting,wait....         ' 
		fi
		sleep 2
		(( t++ ))
	done
}

wait_stop()
{
	t=1
	while [ ${t} -le 10 ] 
	do
 
		status ${server_args}
		check_status=$?
		if [ $check_status -eq $OCF_NOT_RUNNING  ];
		then
			echo -e ${SERVER_NAME} 'stop         ' $SUCCESS_STRING
			return $OCF_SUCCESS
		else
			echo -e ${SERVER_NAME} 'stopping,wait....         ' 
			# return $OCF_ERR_GENERIC
		fi
		sleep 2
		(( t++ ))
	done
}