#!/bin/bash
source $scriptHOME/ocf-returncodes
weblogic_BIN=$weblogicHOME/user_projects/domains/$domains/bin


start_AdminServer()
{
    cd ${weblogic_BIN}
	nohup ./startWebLogic.sh & > /dev/null
	cd -
    return $?
}

stop_AdminServer()
{
    ps -ef | grep 'AdminServer'  |grep -v 'grep' |grep -v '/bin/bash'| grep -v 'script/weblogicAdminServer'|awk '{print $2}' | while read pid   >/dev/null
            do
                    echo "kill "$pid
                    kill -9 $pid
            done 
    return $?
}

status_port()
{
    netstat -na |grep -v 'grep' |grep -v '/bin/bash' |grep 7001  |grep 'LISTEN'  >/dev/null
    return $?
}
status_process()
{
    ps -ef |grep -v 'grep' |grep -v '/bin/bash' | grep -v 'script/weblogicAdminServer'| grep 'AdminServer' >/dev/null
    return $?
}
status_AdminServer()
{
    status_port
    port_status_code=$?
    status_process
    process_status_code=$?
    # echo 'status_AdminServer: port_status_code' $port_status_code ': process_status_code'  $process_status_code
    if [ $port_status_code -eq 0 ] && [ $process_status_code -eq 0 ] ; then
        return $OCF_SUCCESS
    else
        return $OCF_NOT_RUNNING
     fi
 }
 validate() {

    if [ ! -d "${weblogic_BIN}" ] ; then
        echo  "The weblogic floder ${weblogic_BIN} is not in this server!"
        exit 1
    fi
    return 0
}

wait_start()
{
	t=1
	while [ ${t} -le 10 ] 
	do
		 status_AdminServer
		if [ $? -eq $OCF_SUCCESS ]; then
				echo -e 'AdminServer ' 'start         ' $SUCCESS_STRING
				return $OCF_SUCCESS
			else
				echo -e 'AdminServer ' 'is starting, wait.....'
		fi
		sleep 2
		(( t++ ))
	done
	echo 'start timeout in ' 200 's'
}
wait_stop()
{
	t=1
	while [ ${t} -le 10 ] 
	do
		status_AdminServer
		check_status=$?
		if [ $check_status -eq $OCF_NOT_RUNNING  ];
			then
				echo -e 'AdminServer ' 'stop         ' $SUCCESS_STRING
				return $OCF_SUCCESS
			else
				echo -e 'AdminServer ' 'stopping       ' 
		fi
		sleep 2
		(( t++ ))
	done
	echo 'stop timeout in ' 200 's'
}
