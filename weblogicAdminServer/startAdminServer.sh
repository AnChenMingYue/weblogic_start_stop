#!/bin/bash
source $scriptHOME/ocf-returncodes
source $scriptHOME/weblogicAdminServer/fuctionAdminServer.sh


validate
if [ $? -eq 1 ] ;
    then
        return $OCF_NOT_RUNNING
fi

status_AdminServer
if [ $? -eq $OCF_SUCCESS ]; then
		echo -e 'AdminServer ' 'aleady         ' $RUNNING
		return $OCF_SUCCESS
else
    echo 'AdminServer not runing ,try start AdminServer'
    start_AdminServer

	wait_start
fi