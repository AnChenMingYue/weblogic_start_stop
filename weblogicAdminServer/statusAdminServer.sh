#!/bin/bash
source $scriptHOME/ocf-returncodes
source $scriptHOME/weblogicAdminServer/fuctionAdminServer.sh

validate
if [ $? -eq 1 ] ;
    then
        return $OCF_NOT_RUNNING
fi

status_AdminServer

 if [ $? -eq $OCF_SUCCESS ] ; 
then
	echo -e 'Admin Server' 'is                 ' $RUNNING
    return $OCF_SUCCESS
else
	echo -e 'Admin Server' 'is                 ' $STOP 
    return $OCF_NOT_RUNNING
fi
