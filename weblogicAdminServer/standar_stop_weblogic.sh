#!/bin/sh

# WARNING: This file is created by the Configuration Wizard.
# Any changes to this script may be lost when adding extensions to this configuration.
standar_stop_weblogic(){

	wlsUserID="$1"
	wlsPassword="$2"
	ADMIN_URL="$3"
	export wlsUserID
	userID="username=wlsUserID,"
	shift

	
	export wlsPassword
	password="password=wlsPassword,"
	shift


# set ADMIN_URL

	
	shift


# Call setDomainEnv here because we want to have shifted out the environment vars above

DOMAIN_HOME=$weblogicHOME"/user_projects/domains/"$domains

# Read the environment variable from the console.
doExit="true"
if [ "${doExit}" = "true" ] ; then
	exitFlag="doExit"
else
	exitFlag="noExit"
fi

. ${DOMAIN_HOME}/bin/setDomainEnv.sh ${exitFlag}

umask 026


echo "wlsUserID = java.lang.System.getenv('wlsUserID')" >"shutdown.py" 
echo "wlsPassword = java.lang.System.getenv('wlsPassword')" >>"shutdown.py" 
echo "connect(${userID} ${password} url='${ADMIN_URL}', adminServerName='${SERVER_NAME}')" >>"shutdown.py" 
echo "shutdown('${SERVER_NAME}','Server', ignoreSessions='true')" >>"shutdown.py" 
echo "exit()" >>"shutdown.py" 

echo "Stopping Weblogic Server..."

${JAVA_HOME}/bin/java -classpath ${FMWCONFIG_CLASSPATH} ${MEM_ARGS} ${JVM_D64} ${JAVA_OPTIONS} weblogic.WLST shutdown.py  2>&1 

echo "Done"

echo "Stopping Derby Server..."

if [ "${DERBY_FLAG}" = "true" ] ; then
	. ${WL_HOME}/common/derby/bin/stopNetworkServer.sh  >"${DOMAIN_HOME}/derbyShutdown.log" 2>&1 
	echo "Derby server stopped."
fi

# Exit this script only if we have been told to exit.

if [ "${doExitFlag}" = "true" ] ; then
	return
fi
}
