#!/bin/bash
source $scriptHOME/ocf-returncodes
source $scriptHOME/weblogicAdminServer/fuctionAdminServer.sh


validate
if [ $? -eq 1 ] ;
    then
        return $OCF_NOT_RUNNING
fi

status_AdminServer
check_status=$?
# echo 'stop dminServer ,check_status ' $check_status
if [ $check_status -eq $OCF_NOT_RUNNING ] ; 
then
	echo -e 'AdminServer ' 'stop         ' $SUCCESS_STRING
    return $OCF_SUCCESS
else
    stop_AdminServer
    # sleep 10s
	
	wait_stop
   
fi
